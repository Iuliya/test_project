import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { Table } from './components/Table'
import  Fam  from './components/Fam'
import REST_funcs from './REST_funcs';

class App extends Component {

  state = {  
  fam: '',
  nam:'',
  data: [],
  }

  getTest = (е) => {   	    
    REST_funcs.getdata(this.state.fam, (a) => {     
      this.setState({data:a});
    }, (err_s) => { console.log(err_s) });
   
   }

  onChangeParam = (e) => {   
    this.state.fam = e.target.value; 
   }
  render() {    
    return (
      <div className="App">        
        <div className="container mrgnbtm">
          <div className="row">
            <div className="col-md-8">
                <Fam                 
                  onChangeParam={this.onChangeParam}
                  getTest={this.getTest}           
                  >
                </Fam>
            </div>           		
          </div>
        </div>       
        <div className="row mrgnbtm">
          <Table data={this.state.data}></Table>         
        </div>
      </div>
    );
  }
}

export default App;
