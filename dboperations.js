var config = require('./config/mssql.config');
const sql = require('mssql');
const { request } = require('express');


async function get_db_Query(s) {
    try {
        let pool = await sql.connect(config);
        let q = await pool.request().query(s);
        return q.recordsets;
    }
    catch (error) {
        console.log(error);
    }
}

async function get_db_Query_Arr(s,sp) {
    try {
        let pool = await sql.connect(config);
        let result = await pool.request() 
            .input('par1', sql.Int, sp)
            .query(s);

        return result.recordsets;
    }
    catch (error) {
        console.log(error);
    }
}


async function get_db_Query_Arr_str(s,sp) {
    try {
        let pool = await sql.connect(config);
        let result = await pool.request() 
            .input('par1', sql.VarChar, sp)
            .query(s);

        return result.recordsets;
    }
    catch (error) {
        console.log(error);
    }
}

module.exports = {
    get_db_Query: get_db_Query,  
    get_db_Query_Arr: get_db_Query_Arr,  
    get_db_Query_Arr_str: get_db_Query_Arr_str,
  
} 